function checkPassword() {
  function checkCharacters(pwdToCheck, doesCharFulfillCondition) {
    for (let i = 0; i < pwdToCheck.length; i++) {
      const char = pwdToCheck[i];
      if (doesCharFulfillCondition(char)) {
        return true;
      }
    }
    return false;
  }

  function createItemAndAddToList(elList, text) {
    const elItem = document.createElement('li');
    elItem.textContent = text;
    elList.appendChild(elItem);
  }

  // Webseite nach Durchführung von Funktion nicht neu laden
  event.preventDefault();

  // Passwort einlesen
  const elInput = document.getElementById('pwd-to-check');
  const pwdToCheck = elInput.value;

  // überprüfen, ob etwas eingegeben wurde
  if (pwdToCheck) {
    // Ist Passwort genügend lang?
    const isLongEnough = pwdToCheck.length >= 10;
    // beinhaltet Passwort Kleinbuchstaben?
    const doesContainUpper = checkCharacters(pwdToCheck, (char) => {
      return !parseInt(char) && char.toUpperCase() === char;
    });
    // beinhaltet Passwort Grossbuchstaben?
    const doesContainLower = checkCharacters(pwdToCheck, (char) => {
      return !parseInt(char) && char.toLowerCase() === char;
    });
    // beinhaltet Passwort Zahlen?
    const doesContainNumber = checkCharacters(pwdToCheck, (char) => {
      if (parseInt(char)) {
        return true;
      }
      return false;
    });

    // Element für Ausgabe selektieren
    const elOutput = document.getElementById('output-check');
    // Element anzeigen
    elOutput.classList.remove('do-not-display');
    // Erfüllt Passwort alle Bedingungen?
    if (
      isLongEnough &&
      doesContainUpper &&
      doesContainLower &&
      doesContainNumber
    ) {
      // Resultat des Checks in Absatz für Ausgabe schreiben
      elOutput.textContent = 'Passwort erfüllt alle Bedingungen';
      // Hinzufügen von Klasse für korrektes Passwort
      elOutput.setAttribute('class', 'right');
    } else {
      // Resultat des Checks in Absatz für Ausgabe schreiben
      elOutput.textContent = 'Passwort erfüllt folgende Bedingungen nicht:';
      // Hinzufügen von Klasse für korrektes Passwort
      elOutput.setAttribute('class', 'wrong');
      // Listen Element für nicht erfüllte Bedingungen erstellen
      const elList = document.createElement('ul');
      // Listen Items für nicht erfüllte Bedingungen zu Liste hinzufügen
      if (!isLongEnough) {
        createItemAndAddToList(elList, 'Ist nicht mind. 10 Zeichen lang');
      }
      if (!doesContainUpper) {
        createItemAndAddToList(elList, 'Beinhaltet keine Grossbuchstaben');
      }
      if (!doesContainLower) {
        createItemAndAddToList(elList, 'Beinhaltet keine Kleinbuchstaben');
      }
      if (!doesContainNumber) {
        createItemAndAddToList(elList, 'Beinhaltet keine Zahlen');
      }
      // Liste zu Element für Ausgabe hinzufügen
      elOutput.appendChild(elList);
    }
    // Eingabe löschen
    elInput.value = '';
  }
}

function createInputFields() {
  function createInputField(attrNameValues) {
    const elInput = document.createElement('input');
    for (let i = 0; i < attrNameValues.length; i++) {
      const attrName = attrNameValues[i].attrName;
      const attrValue = attrNameValues[i].attrValue;
      elInput.setAttribute(attrName, attrValue);
    }
    return elInput;
  }

  // Formular selktieren
  const elForm = document.getElementById('password-form');
  // Passwort Feld erstellen
  const elPwdInput = createInputField([
    { attrName: 'type', attrValue: 'password' },
    { attrName: 'id', attrValue: 'pwd-to-check' },
    { attrName: 'name', attrValue: 'pwd-to-check' },
    { attrName: 'placeholder', attrValue: 'Passwort eingeben' },
  ]);
  // Passwort Feld Formular hinzufügen
  elForm.appendChild(elPwdInput);
  // Submit Feld erstellen
  const elSubmitInput = createInputField([
    { attrName: 'type', attrValue: 'submit' },
    { attrName: 'id', attrValue: 'check-pwd' },
    { attrName: 'name', attrValue: 'check-pwd' },
    { attrName: 'value', attrValue: 'Passwort prüfen' },
    { attrName: 'onclick', attrValue: 'checkPassword()' },
  ]);
  // Submit Feld Formular hinzufügen
  elForm.appendChild(elSubmitInput);
}

createInputFields();
